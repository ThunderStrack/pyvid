import collections.abc
from datetime import datetime, timedelta


def get_current_date_string():
    return str(datetime.today().strftime("%Y-%m-%d"))


def get_yesterdays_date_string():
    return datetime.strftime(datetime.now() - timedelta(1), "%Y-%m-%d")


def get_start_date():
    return datetime(2020, 1, 1)


def get_current_date():
    return datetime.today()


def get_long_date_string(date):
    return str(datetime.strftime(date, "%Y-%m-%d"))


def get_firebase_url():
    return 'https://pyvid-24.firebaseio.com'


def update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d
