import requests
from firebase import firebase as fb
from bs4 import BeautifulSoup as bs

import util
import creds

db = None


def main():
    global db
    db = init_firebase()

    data = get_data()
    date = util.get_yesterdays_date_string()  # Data is always one day behind

    db.put('/state/at/rkgraz/', date, data)


def get_data():
    # Start the session
    session = requests.Session()

    s = session.get('https://start.st.roteskreuz.at/')
    soup = bs(s.text, 'html.parser')
    req_verify_token = str(soup.findAll('input', {"type": "hidden"})[0]['value'])

    # Create the payload
    u, p = creds.getCredentials()
    payload = {
        'Username': u,
        'Password': p,
        '__RequestVerificationToken': req_verify_token,
        'X-Requested-With': 'XMLHttpRequest'
    }

    # Post the payload to the site to log in
    login_url = "https://start.st.roteskreuz.at/Anmelden"
    session.post(login_url,
                 data=payload,
                 headers={
                     "Host": "portal.st.roteskreuz.at",  # mandatory for successful auth
                 })

    target_url = 'https://start.st.roteskreuz.at/'
    # Navigate to the next page and scrape the data
    s = session.get(target_url, headers=dict(referer=target_url))

    soup = bs(s.text, 'html.parser')
    htmls = soup.findAll('div', {"class": "stats-content"})
    data_items = []

    for h in htmls:
        data_items.append({
            "metric": h.findAll("div", {"class": "stats-title"})[0].text,
            "value": int(h.findAll("div", {"class": "stats-number"})[0].text)
        })

    return data_items


def init_firebase():
    global db
    return fb.FirebaseApplication(util.get_firebase_url(), None)


if __name__ == '__main__':
    main()
