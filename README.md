# pyVID

A Small Data Analysis Script for the COVID Situation in Austria and the whole world

## Data Origin

The Austrian Data is taken from [Austrian Gesundheitsministerium](https://info.gesundheitsministerium.at/).
Special thanks to their opennes, in providing this data ;)

The international data is taken from [John Hopkins University](https://coronavirus.jhu.edu/map.html)
via the [mathdroids API](https://covid19.mathdro.id/api/).

## Data Aggregation

### Austria

Currently, you can crawl the websites of the Austrian ministry of health. 
The data is aggregated in a FireBase Realtime Database. This ensures the 
longevity of the Austrian COVID data as the ministry limits the trend to the 
last 30 days. 

To produce trends of the individual counties, hospitalizations and test numbers,
also these numbers are stored once per day.

### International

All international data is aggregated from John Hopkins University. They seem to 
be quite up to date with the individual countries' data. At least in Europe, in the
US and in China.

Currently, for each country except Austria, the numbers of confirmed infections, 
recoveries and COVID correlated* deaths are stored on  a daily basis.
Futhermore, the maximum daily increase per country is stored as well.  

## Crawler

The crawler aggregates all available Austrian COVID data in a FireBase Realtime Database.
Currently, one has to manually start the crawler at least once per day to get consistent
data. It is planned to install the crawler on a server to make it run automatically.

## Analysis

Currently, one can produce several graphs from the aggregated data. Please run 
the `pyvid.py` (in an IDE) to see the graphs produced by `matplotlib`.

## Setup:

Download the scripts:

```bash
git clone git@gitlab.com:ThunderStrack/pyvid.git
```  

In case you want to setup a virtual environment, run:

```bash
python -m venv venv
source venv/bin/activate
```

Use `pip` to install all dependencies:

```bash
pip install -r requirements.txt
```

Now you're good to go.

## Run

Run the following command to start the crawlers

```bash
#Austria Data Crawler
python austriaCrawler.py

#International Data Crawler
python jhuCrawler.py
```

Run the following commands to get the pyVID graphs:

```bash
python pyvid.py
```

Note that currently it is not tested if the graphs created by `matplotlib`
are rendered correctly when `pyvid` is executed in the terminal.
It is recommended to run it with an IDE such as pyCharm.