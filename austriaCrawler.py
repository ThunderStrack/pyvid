import requests
import json
import numpy as np
from firebase import firebase as fb
from bs4 import BeautifulSoup as bs
import csv

import util

db = None


def main():
    global db

    print("Crawler: Initializing DB Connection")
    db = init_firebase()

    print("Crawler: Updating State Info")
    update_countries()

    print("Crawler: Updating Current AT numbers")
    #update_general_data()

    # delete_old_infected_data()

    print("Crawler: Updating AT Trend Numbers")
    update_trend_data()

    # migrate_county_data()

    print("Crawler: Updating County Numbers")
    update_county_data()

    # migrate_test_trend_data()

    print("Crawler: Updating Test Trend Numbers")
    update_test_trend_data()

    print("Crawler: Updating Vaccination Data")
    update_vaccination_data()


def update_general_data():
    global db
    nr_infections, date = get_today_data()
    nr_tests = get_nr_tests()
    data = {
        'nr_infections': nr_infections,
        'last_refresh': date.strip("\""),
        'nr_tests': nr_tests
    }
    db.put('/state/at/', 'today', data)


def delete_old_infected_data():
    previous_trend = db.get('/state/at/trend/infected', '')
    new_trend = {}
    for key, value in previous_trend.items():
        if key.startswith('2020-'):
            new_trend[key] = value

    db.put('/state/at/trend', 'infected', new_trend)


def update_trend_data():
    infection_trend = get_trend_data()

    nr_recoveries, nr_deaths = get_recovery_death_data()
    nr_infections_today = infection_trend[len(infection_trend) - 1]["y"]
    today = util.get_current_date_string()

    previous_trend = db.get('/state/at/trend', '') or {}

    infection_data = previous_trend.get("infected") or {}
    recovery_data = previous_trend.get("recovered") or {}
    death_data = previous_trend.get("dead") or {}

    # 2021 Migration
    #new_infection_data = {}
    #new_recovery_data = {}
    #new_death_data = {}

    #for key, value in infection_data.items():
    #    new_infection_data['2020-' + key] = value
    #for key, value in recovery_data.items():
    #    new_recovery_data['2020-' + key] = value
    #for key, value in death_data.items():
    #    new_death_data['2020-' + key] = value

    #new_infection_data[today] = nr_infections_today
    #new_recovery_data[today] = nr_recoveries
    #new_death_data[today] = nr_deaths

    infection_data[today] = nr_infections_today
    recovery_data[today] = nr_recoveries
    death_data[today] = nr_deaths

    for day in infection_trend:
        infection_data[day["label"]] = day["y"]

    trend = previous_trend or {}
    trend["infected"] = infection_data
    trend["recovered"] = recovery_data
    trend["dead"] = death_data

    #trend["infected"] = new_infection_data
    #trend["recovered"] = new_recovery_data
    #trend["dead"] = new_death_data

    db.put('/state/at/', 'trend', trend)


def migrate_county_data():
    previous_county_data = db.get('/state/at/counties', '')
    new_county_data = {}
    for key, value in previous_county_data.items():
        new_county_data['2020-' + key] = value
    db.put('/state/at//', 'counties', new_county_data)


def update_county_data():
    inhabitants, _ = get_county_inhabitants()
    county_infections, county_deaths, county_recoveries, hospitalizations, icu = get_county_numbers()

    today = util.get_current_date_string()

    previous_county_data = db.get('/state/at/counties/' + today, '')

    counties = ['Wien', 'Niederösterreich', 'Oberösterreich', 'Steiermark',
                'Tirol', 'Kärnten', 'Salzburg', 'Vorarlberg', 'Burgenland']
    counties_short = ['W', 'NÖ', 'OÖ', 'Stmk', 'T', 'Ktn', 'Sbg', 'Vbg', 'Bgld']

    data = previous_county_data or {short: {} for short in counties_short}

    for i in range(len(counties)):
        county = counties[i]
        short = counties_short[i]

        data[short]["nameShort"] = short
        data[short]["name"] = county
        data[short]["inhabitants"] = inhabitants[short]
        data[short]["hospitalizations"] = hospitalizations[short]
        data[short]["icu"] = icu[short]
        data[short]["infections"] = county_infections[short]
        data[short]["recovered"] = county_recoveries[short]
        data[short]["dead"] = county_deaths[short]

    db.put('/state/at/counties', today, data)


def migrate_test_trend_data():
    previous_trend_data = db.get('/state/at/tests', '')
    new_trend_data = {}
    for key, value in previous_trend_data.items():
        new_trend_data['2020-' + key] = value
    db.put('/state/at/', 'tests', new_trend_data)


def update_test_trend_data():
    nr_tests = get_nr_tests()
    today = util.get_current_date_string()

    data = {
        "nr_tests": nr_tests
    }

    db.put('/state/at/tests', today, data)


def update_countries():
    global db

    countries = db.get('/state', '')
    non_existent = countries is None

    if non_existent:
        lst_countries = []
    else:
        lst_countries = [country["name"] for key, country in countries.items()]

    if non_existent or "Austria" not in lst_countries:
        at = {
            'name': 'Austria',
            'short': 'AT'
        }
        db.put('/state/', 'at', at)


def update_vaccination_data():
    res_raw = requests.get(url="https://info.gesundheitsministerium.at/data/national.csv")
    db.put('/state/at', 'vaccinations', {'txt': res_raw.text})


def init_firebase():
    global db
    return fb.FirebaseApplication(util.get_firebase_url(), None)


def get_trend_data():
    #res = requests.get(url="https://info.gesundheitsministerium.at/data/Epikurve.js")
    #json_res = json.loads(res.text.split("=")[1].strip().split("];")[0] + "]")

    res_raw = requests.get(url="https://covid19-dashboard.ages.at/data/JsonData.json")
    json_res_raw = json.loads(res_raw.text)
    json_res_timeline = json_res_raw["CovidFaelle_Timeline"]
    json_res_timeline_at = list(filter(lambda el: el["BundeslandID"] == 10, json_res_timeline))
    infection_trend = []

    for i in range(0, len(json_res_timeline_at)):
        day = str(json_res_timeline_at[i]["Time"])[:10]
        infection_trend.append({
            "label": day,
            "y": json_res_timeline_at[i]["AnzahlFaelleSum"]
        })

    return infection_trend


def get_county_data():
    res = requests.get(url="https://info.gesundheitsministerium.at/data/Bundesland.js")
    json_res = res.text.split("=")[1].strip().split(";")[0]
    return json.loads(json_res)


def get_today_data():
    res = requests.get(url="https://info.gesundheitsministerium.at/data/SimpleData.js")
    nr_infections = int(res.text.split("\n")[0].split("=")[1].split(";")[0].strip().replace("\"", ""))
    last_refreshed = res.text.split("\n")[1].split("=")[1].split(";")[0].strip()
    return nr_infections, last_refreshed


def get_county_inhabitants():
    inh = {
        "W": 1911728,
        "NÖ": 1684623,
        "OÖ": 1490392,
        "Stmk": 1246576,
        "T": 757852,
        "Ktn": 561390,
        "Sbg": 558479,
        "Vbg": 397094,
        "Bgld": 294466
    }

    inh_sum = np.sum(np.array(inh))
    return inh, inh_sum


def get_county_numbers():
    rows = get_austria_csv_timeline()[1:]
    last_rows = rows[len(rows) - 10: len(rows) - 1]

    l_infections = [int(row[3]) for row in last_rows]
    l_deaths = [int(row[4]) for row in last_rows]
    l_recovered = [int(row[5]) for row in last_rows]
    l_hosp = [int(row[6]) for row in last_rows]
    l_icu = [int(row[7]) for row in last_rows]

    l_counties = ["Bgld", "Ktn", "NÖ", "OÖ", "Sbg", "Stmk", "T", "Vbg", "W"]
    infections, deaths, recovered, hospitalizations, icu = {}, {}, {}, {}, {}

    for i in range(0, len(l_counties)):
        infections[l_counties[i]] = l_infections[i]
        deaths[l_counties[i]] = l_deaths[i]
        recovered[l_counties[i]] = l_recovered[i]
        hospitalizations[l_counties[i]] = l_hosp[i]
        icu[l_counties[i]] = l_icu[i]

    return infections, deaths, recovered, hospitalizations, icu


def get_nr_tests():
    rows = get_austria_csv_timeline()[1:]
    last_row = rows[len(rows) - 1]

    return int(last_row[8])


def get_recovery_death_data():
    _, p_recoveries, p_deaths = get_recovery_death_text()
    return int(p_recoveries), int(p_deaths)


def get_recovery_death_text():
    rows = get_austria_csv_timeline()[1:]

    last_austria_row = list(filter(lambda x: x[1] == '10', rows))
    last_austria_row = last_austria_row[len(last_austria_row) - 1]

    return last_austria_row[3], last_austria_row[5], last_austria_row[4]


def get_austria_csv_timeline():
    #  0 Datum
    #  1 BundeslandID
    #  2 Name
    #  3 BestaetigteFaelleBundeslaende
    #  4 Todesfaelle
    #  5 Genesen
    #  6 Hospitalisierung
    #  7 Intensivstation
    #  8 Testungen
    #  9 TestungenPCR
    # 10 TestungenAntigen
    csv_str = requests.get("https://info.gesundheitsministerium.gv.at/data/timeline-faelle-bundeslaender.csv") \
                  .content.decode('utf-8')[1:].splitlines()
    csv_reader = csv.reader(csv_str, dialect='excel', delimiter=';')
    return [row for row in csv_reader]

if __name__ == '__main__':
    main()
