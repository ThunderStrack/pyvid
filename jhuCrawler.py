import requests
import json
from firebase import firebase as fb
import util
from datetime import timedelta
from bs4 import BeautifulSoup as bs


api_url = "https://covid19.mathdro.id/api"
api_url_daily = api_url + "/daily/"
api_url_countries = api_url + "/countries"

countries = []
days = []
data = {"state": {}}
d_states = data["state"]


def main():
    get_countries()
    #get_daily_country_numbers()
    #map_data_to_fb()
    #update_firebase()
    update_daily_numbers()
    #update_population_numbers()
    #update_countries_alldays()


def get_daily_country_numbers():
    date = util.get_start_date()
    str_date = util.get_long_date_string(date)
    #str_today = '2020-05-31'
    str_today = util.get_long_date_string(util.get_current_date())
    ds = []

    while str_date != str_today:
        ds.append(str_date)
        date = date + timedelta(days=1)
        str_date = util.get_long_date_string(date)
    ds.append(str_date)

    for d in ds:
        print("> fetching data for", d)
        res = json.loads(requests.get(url=api_url_daily + d).text)
        days.append({"date": d, "data": res})


def map_data_to_fb():
    print("> processing data")

    def getShort(country):
        for c in countries:
            if c["name"] == country:
                return c["short"].lower()

    for day in days:
        day["data"].sort(key=lambda d: d["countryRegion"], reverse=False)

        last_country = "Afghanistan"
        country_data = d_states[getShort("Afghanistan")]

        for region in day["data"]:
            current_country = region["countryRegion"].strip()
            current_country_short = getShort(current_country)

            if current_country_short is None or current_country == "Austria" or current_country == "Others" or \
                    current_country == "Diamond Princess":
                continue
            elif current_country == "Mainland China":
                current_country = "China"
            elif current_country == "North Macedonia":
                current_country = "Macedonia"
            elif current_country == "South Korea":
                current_country = "Korea, South"
            elif current_country == "Taiwan":
                current_country = "Taiwan*"
            elif current_country == "UK" or current_country == "North Ireland":
                current_country = "United Kingdom"
            elif current_country == "Czech Republic":
                current_country = "Czechia"
            elif current_country == "Burma":
                current_country = "Myanmar"
            elif current_country == "Congo (Brazzaville)":
                current_country = "Congo-Brazzaville"
            elif current_country == "Congo (Kinshasa)":
                current_country = "Dem. Rep. Congo"

            if current_country != last_country:
                if "trend" in d_states[getShort(last_country.strip())]:
                    d_states[getShort(last_country.strip())]["trend"] = country_data["trend"]
                else:
                    d_states[getShort(last_country.strip())]["trend"] = {}
                    d_states[getShort(last_country.strip())]["trend"].update(country_data["trend"])
                country_data = d_states[getShort(current_country)]

            trend = country_data.get("trend") or {"infected": {}, "recovered": {}, "dead": {}}
            #trend["infected"][day["date"][5:]] = (trend["infected"].get(day["date"][5:]) or 0) + int(region.get("confirmed") or 0)
            #trend["recovered"][day["date"][5:]] = (trend["recovered"].get(day["date"][5:]) or 0) + int(region.get("recovered") or 0)
            #trend["dead"][day["date"][5:]] = (trend["dead"].get(day["date"][5:]) or 0) + int(region.get("deaths") or 0)
            trend["infected"][day["date"][0:]] = (trend["infected"].get(day["date"][0:]) or 0) + int(region.get("confirmed") or 0)
            trend["recovered"][day["date"][0:]] = (trend["recovered"].get(day["date"][0:]) or 0) + int(region.get("recovered") or 0)
            trend["dead"][day["date"][0:]] = (trend["dead"].get(day["date"][0:]) or 0) + int(region.get("deaths") or 0)
            if "trend" not in country_data:
                country_data["trend"] = {}
            country_data["trend"].update(trend)

            last_country = current_country


def update_firebase():
    print("> Updating Firebase")
    db = fb.FirebaseApplication(util.get_firebase_url(), None)
    all_data = db.get("/", None)

    # Migration from %m-%d to %y-%m-%d
    # for key, value in all_data["state"].items():
    #    if key == 'at':
    #        continue
    #    all_data["state"][key] = {
    #        "inhabitants": value.get("inhabitants") or 1,
    #        "name": value.get("name"),
    #        "short": value.get("short"),
    #    }

    all_data["state"] = util.update(all_data["state"], data["state"])
    db.put("/", "/", all_data)
    return


def get_countries():
    res = json.loads(requests.get(url=api_url_countries).text)

    for c in res["countries"]:
        if c.get("iso2") is not None and c["iso2"] != "AT":
            country = {"name": c["name"], "short": c["iso2"]}
            countries.append(country)
            d_states[c["iso2"].lower()] = country

    hk = {"name": "Hong Kong", "short": "HK"}
    countries.append(hk)
    d_states["hk"] = hk

    mc = {"name": "Macau", "short": "MC"}
    countries.append(mc)
    d_states["mc"] = mc

    md = {"name": "Macedonia", "short": "MD"}
    countries.append(md)
    d_states["md"] = md

    fo = {"name": "Faroe Islands", "short": "FO"}
    countries.append(fo)
    d_states["fo"] = fo

    gi = {"name": "Gibraltar", "short": "GI"}
    countries.append(gi)
    d_states["gi"] = gi

    mm = {"name": "Myanmar", "short": "MM"}
    countries.append(mm)
    d_states["mm"] = mm

    cv = {"name": "Cabo Verde", "short": "CV"}
    countries.append(cv)
    d_states["cv"] = cv

    cd = {"name": "Congo-Brazzaville", "short": "CD"}
    countries.append(cd)
    d_states["cd"] = cd

    cg = {"name": "Dem. Rep. Congo", "short": "CG"}
    countries.append(cg)
    d_states["cg"] = cg

    ci = {"name": "Cote d\'Ivoire", "short": "CI"}
    countries.append(ci)
    d_states["ci"] = ci


def update_daily_numbers():
    db = fb.FirebaseApplication(util.get_firebase_url(), None)
    today = util.get_current_date_string()
    all_data = db.get("/state", None)

    for country in countries:
        iso2 = country["short"]
        name = country["name"]
        print("> updating", name)

        res = json.loads(requests.get(url=api_url_countries + "/" + name).text)
        if "error" in res:
            print(">    Name unknown, trying iso2")
            res = json.loads(requests.get(url=api_url_countries + "/" + iso2).text)
            if "error" in res:
                print(">    ISO2 unknown, too. Not updating", name)
                continue

        daily_data = {
            "infected": {
                today: res["confirmed"]["value"]
            },
            "recovered": {
                today: res["recovered"]["value"]
            },
            "dead": {
                today: res["deaths"]["value"]
            }
        }

        if all_data.get(iso2.lower()) is None:
            all_data.update({
                iso2.lower(): {
                    "name": name,
                    "short": iso2,
                    "trend": {
                        "infected": {
                            today: 0
                        },
                        "recovered": {
                            today: 0
                        },
                        "dead": {
                            today: 0
                        }
                    }
                }
            })


        all_data[iso2.lower()]["trend"]["infected"].update(daily_data["infected"])
        all_data[iso2.lower()]["trend"]["recovered"].update(daily_data["recovered"])
        all_data[iso2.lower()]["trend"]["dead"].update(daily_data["dead"])

    db.put("/", "state", all_data)


def update_population_numbers():
    db = fb.FirebaseApplication(util.get_firebase_url(), None)
    today = util.get_current_date_string()
    html = requests.get("https://en.wikipedia.org/wiki/"
                        "List_of_countries_by_population_(United_Nations)").content
    table = bs(html).findAll('table')[3]
    rows = table.findAll('tr')

    pop = {}
    for row in rows[1:len(rows) - 2]:
        name = row.findAll('td')[0].find('a').text
        inh = int(row.findAll('td')[4].text.replace(",", ""))
        pop[name] = inh

    all_data = db.get("/state", None)

    for country in countries:
        iso2 = country["short"]
        name = country["name"]
        print("> updating", name)

        res = json.loads(requests.get(url=api_url_countries + "/" + name).text)
        if "error" in res:
            print(">    Name unknown, trying iso2")
            res = json.loads(requests.get(url=api_url_countries + "/" + iso2).text)
            if "error" in res:
                print(">    ISO2 unknown, too. Not updating", name)
                continue

        if name == "Czechia" :
            inh = pop["Czech Republic"]
        elif name == "Korea, South":
            inh = pop["South Korea"]
        elif name == "Kosovo":
            inh = 1793000
        elif name == "Serbia":
            inh = 8737371
        elif name == "Sao Tome and Principe":
            inh = pop["São Tomé and Príncipe"]
        elif name == "Taiwan*":
            inh = pop["Taiwan"]
        elif name == "US":
            inh = pop["United States"]
        elif name == "Macedonia":
            inh = pop["North Macedonia"]
        elif name == "Cabo Verde":
            inh = pop["Cape Verde"]
        elif name == "Cote d\'Ivoire":
            inh = pop["Ivory Coast"]
        else:
            inh = pop[name]

        all_data[iso2.lower()]["inhabitants"] = inh

    db.put("/", "state", all_data)


if __name__ == '__main__':
    main()
