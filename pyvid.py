import json
import requests
import matplotlib.pyplot as plt
import numpy as np
from firebase import firebase as fb

import util

data = {}


def main():
    get_austria_data()

    plot_trend()
    plot_county_distribution()
    plot_relative_county_distribution()
    plot_absolute_daily_increase()
    plot_relative_daily_increase()
    plot_test_trend()
    plot_county_infection_death_ratio()


def plot_trend():
    i_days, nr_infections = get_trend_data("infected")
    r_days, nr_recoveries = get_trend_data("recovered")
    d_days, nr_deaths = get_trend_data("dead")
    s_days, nr_sick = get_currently_sick_trend_data()

    plt.plot(i_days, nr_infections, label="All Confirmed Infections", color="blue")
    plt.plot(r_days, nr_recoveries, label="Recoveries", color="green")
    plt.plot(d_days, nr_deaths, label="Deaths", color="red")
    plt.plot(s_days, nr_sick, label="Currently Sick", linestyle=":", color="orange")

    plt.title("Corona Infections Trend in AT")
    plt.xlabel("Day")
    plt.ylabel("Infected People")
    plt.xticks(rotation=60)
    plt.legend()
    fig = plt.gcf()
    fig.set_size_inches(10, 7, forward=True)
    plt.show()


def plot_test_trend():
    t_days, nr_tests = get_test_trend()
    i_days, nr_infections = get_trend_data("infected")

    plt.plot(i_days, nr_infections, label="All confirmed infections",
             color="red", linestyle=":")
    plt.plot(t_days, nr_tests, label="All conducted tests", color="blue")

    plt.title("Test Trend in AT")
    plt.xlabel("Day")
    plt.ylabel("Nr. of tests/infections")
    plt.xticks(rotation=60)
    plt.legend()
    fig = plt.gcf()
    fig.set_size_inches(10, 7, forward=True)
    plt.show()


def plot_county_distribution():
    names, nr_infections = get_county_data()

    explode = (0.0, 0.0, 0.05, 0.0, 0.0, 0.0, 0.05, 0.0, 0.05)
    fig1, ax1 = plt.subplots()
    ax1.pie(nr_infections, labels=names, autopct='%1.1f%%', shadow=False,
            startangle=0, explode=explode)
    ax1.axis('equal')
    plt.title("Absolute Distribution of COVID infections in AT")
    plt.show()


def plot_relative_county_distribution():
    county_names, nr_infections = get_county_data()
    inhabitants, all_inh = get_county_inhabitants()
    current_nr_infections, date = get_today_data()

    rel_nrs = []
    for i in range(len(nr_infections)):
        rel_nrs.append((nr_infections[i] / current_nr_infections) / (inhabitants[i] / all_inh))

    explode = (0.0, 0.0, 0.0, 0.00, 0.05, 0.0, 0.05, 0.05, 0.0)
    fig1, ax1 = plt.subplots()
    ax1.pie(rel_nrs, labels=county_names, autopct='%1.1f%%', shadow=False,
            startangle=0, explode=explode)
    ax1.axis('equal')
    plt.title("Relative Distribution of COVID infections in AT")
    plt.show()


def plot_absolute_daily_increase():
    days, nr_infections = get_trend_data("infected")

    increases = [0, ]
    for i in range(1, len(nr_infections)):
        increases.append(nr_infections[i] - nr_infections[i - 1])

    y_pos = np.arange(len(increases))
    plt.bar(y_pos, increases, align='center', alpha=0.5)
    plt.xticks(y_pos, days, rotation=60)
    plt.ylabel('Increase')
    plt.title('Absolute Increase of Infections per Day')
    fig = plt.gcf()
    fig.set_size_inches(10, 7, forward=True)
    plt.show()


def plot_relative_daily_increase():
    days, nr_infections = get_trend_data("infected")

    rel_increase = [0, 0, ]
    for i in range(2, len(nr_infections)):
        rel_increase.append(nr_infections[i] / nr_infections[i - 1] - 1.0)

    y_pos = np.arange(len(rel_increase))
    plt.bar(y_pos, rel_increase, align='center', alpha=0.5)
    plt.xticks(y_pos, days, rotation=60)
    plt.ylabel('Increase')
    plt.title('Relative Increase of Infections per Day')
    fig = plt.gcf()
    fig.set_size_inches(10, 7, forward=True)
    plt.show()


def plot_county_infection_death_ratio():
    counties, drs = get_county_infection_death_ratio_data()
    idr_mean = np.array(drs).mean()

    y_pos = np.arange(len(counties))
    plt.bar(y_pos, drs, align='center', alpha=0.8)
    plt.xticks(y_pos, counties, rotation=45)
    plt.ylabel('Infection / Death Ratio')
    plt.ylim(0, .085)
    plt.title('County (Bundesland)')
    plt.plot([-1, 9], [idr_mean for _ in range(2)], linestyle=":",
             color="darkred", label="Average")
    plt.legend()
    plt.text(-.5, .05, "Infection / Death Ratio:\n#deaths / #infections",
             bbox={'facecolor': 'lightblue', 'alpha': 0.3, 'pad': 10})
    plt.show()


def get_trend_data(type):
    global data
    trend_data = data["trend"][type]

    return extract_trend_numbers(trend_data)


def get_currently_sick_trend_data():
    global data
    infected = data["trend"]["infected"]
    recovered = data["trend"]["recovered"]
    dead = data["trend"]["dead"]

    tmp_sick = {}
    for day, nr_infected in infected.items():
        tmp_sick[day] = nr_infected - (recovered.get(day) or 0) - (dead.get(day) or 0)

    return extract_trend_numbers(tmp_sick)


def extract_trend_numbers(dict_data):
    days = [str(key).split("-")[1] + "." + str(key).split("-")[0]
            for key, value in dict_data.items()]
    numbers = [value for key, value in dict_data.items()]
    return days, numbers


def get_county_data():
    global data
    counties = data["counties"][util.get_current_date_string()]
    short_names = [val["nameShort"] for _, val in counties.items()]
    nr_infections = [val["infections"] for _, val in counties.items()]

    return short_names, nr_infections


def get_today_data():
    global data
    today = data["today"]
    return today["nr_infections"], today["last_refresh"]


def get_county_inhabitants():
    global data
    counties = data["counties"][util.get_current_date_string()]
    inh = [val["inhabitants"] for _, val in counties.items()]
    inh_sum = np.sum(np.array(inh))
    return inh, inh_sum


def get_test_trend():
    global data
    days, tests = extract_trend_numbers(data["tests"])
    tests = [val["nr_tests"] for val in tests]
    return days, tests


def get_county_infection_death_ratio_data():
    global data
    county_trend = data["counties"]
    today = util.get_current_date_string()
    counties_today = county_trend[today]
    counties = [key for key, _ in counties_today.items()]
    idrs = [int(c["dead"]) / int(c["infections"]) for _, c in counties_today.items()]

    return counties, idrs


def get_austria_data():
    global data
    db = fb.FirebaseApplication(util.get_firebase_url(), None)
    data = db.get('/state/at', '')


if __name__ == '__main__':
    main()
